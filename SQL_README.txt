### VITALIY TRACH
### YURIY BOIKO
### CPTS 415

## CREATE TABLE STATEMENTS FOR POSTGRES

Create TABLE products(
    asin VARCHAR(10),
    type VARCHAR(5),
    PRIMARY KEY(asin)
);

Create TABLE book(
    asin VARCHAR(10),
    title VARCHAR(400),
    avg_rating float,
    salesrank int,
    FOREIGN KEY(asin) REFERENCES products,
    PRIMARY KEY(asin)
);

Create TABLE dvd(
    asin VARCHAR(10),
    title VARCHAR(400),
    avg_rating float,
    salesrank int,
    FOREIGN KEY(asin) REFERENCES products,
    PRIMARY KEY(asin)
);

Create TABLE video(
    asin VARCHAR(10),
    title VARCHAR(400),
    avg_rating float,
    salesrank int,
    FOREIGN KEY(asin) REFERENCES products,
    PRIMARY KEY(asin)
);

Create TABLE music(
    asin VARCHAR(10),
    title VARCHAR(400),
    avg_rating float,
    salesrank int,
    FOREIGN KEY(asin) REFERENCES products,
    PRIMARY KEY(asin)
);

Create TABLE similarproducts(
    id int,
    asin VARCHAR(10),
    similarasin VARCHAR(10),
	FOREIGN KEY(asin) REFERENCES products,
    PRIMARY KEY(id)
);

Create TABLE customer_transaction(
    id SERIAL PRIMARY KEY,
    cid VARCHAR(14),
    asin VARCHAR(10),
    product_rating int
    FOREIGN KEY(asin) REFERENCES products
);

## COPYING THE CSVs INTO POSTGRES
## REPLACE MY FILE PATH WITH YOUR FILEPATH

COPY products(asin, type) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/products.csv' DELIMITER '^';

COPY similarproducts(id, asin, similarasin) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/similarproducts.csv' DELIMITER '^';

COPY book(asin, title, avg_rating, salesrank) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/book.csv' DELIMITER '^';

COPY music(asin, title, avg_rating, salesrank) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/music.csv' DELIMITER '^';

COPY dvd(asin, title, avg_rating, salesrank) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/dvd.csv' DELIMITER '^';

COPY video(asin, title, avg_rating, salesrank) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/video.csv' DELIMITER '^';

COPY video(cid, asin, product_rating) 
FROM '/Users/vitaliytrach/eclipse-workspace/TextFileParser/resources/customer_transactions.csv' DELIMITER '^';

## QUERIES

#ratingSearch()
SELECT title, asin 
FROM 'TABLE' 
WHERE avg_rating >= 'USER_ENTERED_RATING';

#similaritySearch()
SELECT DISTINCT b.title, t1.asin
FROM similar products s, 'TABLE' t1
WHERE s.similarasin = t1.asin
AND s.asin = 'USER_ENTERED_ASIN';


