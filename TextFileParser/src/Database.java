// Vitaliy Trach
// Yuriy Boiko
// Cpts 415

/*
 * The purpose of this class is to communicate
 * with the local PostgreSQL DBMS, like setting up the 
 * connection, and running queries.
 */

import java.sql.*;
import java.util.ArrayList;


public class Database {
	
	private boolean connected = false;
	private Connection connection = null;
	
	public Database() {

		initConnection();
	}
	
	// Setting up connection to the DB
	private void initConnection() {
		
		// Registering DB driver
		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return;
		}

		// Setting up a connection with PostgreSQL
		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/Amazon", "postgres",
					"password");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		// Checking if connection was successful
		if (connection != null) {
			connected = true;
		} 
	}
	
	// Method to run queries in the DB
	// Param: String query: The query that will be run in the DB
	// Return: a list of strings, that hold the values of the results
	public ArrayList<String> exeQuery(String query) throws SQLException {
		
		ArrayList<String> results = new ArrayList<String>();
		
		if(connected) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			ResultSetMetaData metaData = resultSet.getMetaData();
		
			while(resultSet.next()) {			
				for(int i = 0; i < metaData.getColumnCount(); i++) {
					results.add(resultSet.getString(i + 1));
				}
			}	
				
			resultSet.close();
			statement.close();
		}	
		
		return results;
	}
}
