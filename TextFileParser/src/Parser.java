import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Parser {
	
	public static final String VIDEO_FILE = "resources/video.csv";
	public static final String BOOKS_FILE = "resources/book.csv";
	public static final String DVD_FILE = "resources/dvd.csv";
	public static final String MUSIC_FILE = "resources/music.csv";
	public static final String SIMILAR_FILE = "resources/similarproducts.csv";
	public static final String PRODUCT_FILE = "resources/products.csv";
	public static final String CUSTOMER_TRANSACTIONS = "resources/customer_transactions.csv";
	public static final String AMAZON_FILE = "resources/amazon-meta.txt"; 
	
	public static final int TOTAL_ITEMS = 548552;
	public static final File videos = new File(VIDEO_FILE);
	public static final File dvds = new File(DVD_FILE);
	public static final File music = new File(MUSIC_FILE);
	public static final File books = new File(BOOKS_FILE);	
	public static final File products = new File(PRODUCT_FILE);
	public static final File similarProducts = new File(SIMILAR_FILE);
	public static final File customers_transactions = new File(CUSTOMER_TRANSACTIONS);
	
	
	
	public Parser() {
		try {
			
			if(videos.exists()) {
				videos.delete();
				dvds.delete();
				music.delete();
				books.delete();
				products.delete();
				similarProducts.delete();
				customers_transactions.delete();
			}
			
			videos.createNewFile();
			dvds.createNewFile();
			music.createNewFile();
			books.createNewFile();	
			products.createNewFile();
			similarProducts.createNewFile();
			customers_transactions.createNewFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public void readFile() throws IOException {
		
		
		BufferedReader br = new BufferedReader(new FileReader(AMAZON_FILE));
		  
		System.out.println("Starting to write...");	  
			  
		String line; 
			  
		Schema schema = new Schema(); 
			  		  
		while ((line = br.readLine()) != null) {
			
			//Remove empty space
			line = line.trim();	  		  
			
			//If the next group holds ASIN number
			if(line.contains("ASIN:")) {
				
				//Remove all the garbage
				line = line.replaceAll("ASIN:", "");
				line = line.replaceAll("[\\s+]", "");
				
				//if its a new ASIN the write the old  schema and fill up a new one
				if(schema.getAsin() != line && schema.getAsin() != null) {
					//Write to files
					writeToFile(schema);
				}
				
				//Create new schema
				schema = new Schema();
				//Set new ASIN
				schema.setAsin(line);					  
			}
			//If the next group describes the Title
			else if (line.contains("title:")) {
				
				//Remove the trash
				line = line.replaceAll("title: ", "");
				
				//Add title to the schema
				schema.setTitle(line);					  
			}
			//If the next group about product type 
			else if (line.contains("group:")) {
				
				//Checks what type this product belongs to
				//	Book, Music, DVD or Video
				if(line.contains("Book")) {
					schema.setGroup("Book");
				} else if (line.contains("Music")) {
					schema.setGroup("Music");
				} else if (line.contains("DVD")) {
					schema.setGroup("DVD"); 
				} else {
					schema.setGroup("Video");
				}
					  
			}
			//If the next group is about salesrank
			else if (line.contains("salesrank")) {
				
				//Remove the trash
				line = line.replaceAll("salesrank:", "");
				line = line.replaceAll("[\\s+]", "");
				
				//Add the salesrank to the schema
				schema.setSalesRank(Integer.parseInt(line));
					  
			}
			//If the next group is about similar products
			else if(line.contains("similar:")) {
				
				//Array list to hold all the similar products ASIN number
				ArrayList<String> similar = new ArrayList<String>();
				
				//Remove the trash
				line = line.replaceAll("similar:", "");
				line = line.trim();
				
				//Separate all the similar products ASIN numbers
				String[] temp = line.split("\\s+");
	
				//Iterate through and add them to an ArrayList
				for(int j = 1; j < temp.length; j++) {
					similar.add(temp[j]);
				}
				
				//Add to the schema
				schema.setSimilar(similar);	  					  
			}
			//if the next group is about reviews
			else if (line.contains("reviews:")) {
				//Remove trash and split into sections
				line = line.trim();
				String[] reviewLine = line.split("\\s+");
				
				//Add average rating to the schema
				schema.setAvgRating(Double.parseDouble(reviewLine[reviewLine.length - 1]));				  
				
				//If it has reviews	  
				for(int i = 0; i < Integer.parseInt(reviewLine[4]); i++) {
					
					Review review = new Review();
					
					//Read next line
					line = br.readLine();
					
					//Remove trash and split the the line 
					line = line.trim();
					String[] customerReview = line.split("\\s+");
					
					//Initialize which customer reviewed this product and his rating
					review.setCustomerID(customerReview[2]);
					review.setRating(Integer.parseInt(customerReview[4]));
					
					//add to the schema
					schema.setCustomerTransactions(review);	  					
				}							 
			}		
				  
		}

		writeToFile(schema);
		System.out.println("Finished Writing!");
	}
	
	public void writeToFile(Schema schema) throws IOException {
		
		StringBuilder generalInformation = new StringBuilder();

		//Create the string which writes to the file 
		generalInformation.append(schema.getAsin() + "^");
		generalInformation.append(schema.getTitle() + "^");
		generalInformation.append(schema.getAvgReview() + "^");
		generalInformation.append(schema.getSalesRank() + "\n");
		
		//System.out.println("This is the general info; " + generalInformation);
		//Write to the appropriate file 
		FileWriter generalFileWriter;
		// Books
		if(schema.getGroup() == "Book") {
			generalFileWriter = new FileWriter(books, true);
		} 
		// DVD
		else if (schema.getGroup() == "DVD") {
			generalFileWriter = new FileWriter(dvds, true);
		}
		// Music
		else if (schema.getGroup() == "Music") {
			generalFileWriter = new FileWriter(music, true);
		} 
		// Video
		else {
			generalFileWriter = new FileWriter(videos, true);
		}
		generalFileWriter.write(generalInformation.toString());
		generalFileWriter.close();
		
		
		//String that contains the ASIN product and all its similar ASIN products 
		StringBuilder similarString = new StringBuilder();
		
		ArrayList<String> similar = schema.getSimilar();
		
		for(String string : similar) {
			similarString.append(schema.getAsin() + "^" + string + "\n");
		}
		
		//System.out.println("This is the simalar string :" + similarString);
		
		//Write to similarProducts.csv file
		FileWriter sFileWriter = new FileWriter(similarProducts, true);
		sFileWriter.write(similarString.toString());
		sFileWriter.close();
		
		//String for the product type and their ASIN
		StringBuilder productString = new StringBuilder();
		
		productString.append(schema.getAsin() + "^" + schema.getGroup() + "\n");	
		
		//System.out.println("This is the product string :" + productString);

		FileWriter pFileWriter = new FileWriter(products, true);
		pFileWriter.write(productString.toString());
		pFileWriter.close();
		
		
		//Write to the review file if there are reviews for this schema
		if(schema.getCustomerTransactions()!=null) {
			
			//Arraylist of all reviews for this schema
			ArrayList<Review> list = schema.getCustomerTransactions();
			
			for(int i=0;i<list.size();i++) {
				
				StringBuilder reviewString = new StringBuilder();
				
				//Review of product type and their rating
				Review review = list.get(i);
				
				reviewString.append(review.getCustomerID()+"^"+schema.getAsin()+ "^"+ review.getRating()+"\n");
				
				//System.out.println("This is the review string :" + reviewString);
				
				//write to the customers_transactions.csv file
				FileWriter cFileWriter = new FileWriter(customers_transactions, true);
				cFileWriter.write(reviewString.toString());
				cFileWriter.close();
			}
			//System.out.println("*********Next Product*************");
		}
		
	}
	
}
