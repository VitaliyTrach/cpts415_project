import java.util.ArrayList;

public class Schema {
	
	private int id;
	
	//Amazon Standard Identification Number of this product
	private String asin;
	
	//Title of the product
	private String title;
	
	//Product types: book, DVD, music, video
	private String group;
	
	//Amazon salesrank of this product
	private int salesRank;
	
	//List of all the similar products to this product
	private ArrayList<String> similar = new ArrayList<String>();
	
	//Average rating of this product
	private double avgRating;
	
	//All customers that reviewed this product and their rating
	private ArrayList<Review> customerTransactions= new ArrayList<Review>();
	
	public Schema() {}
	
	// GETTERS AND SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public int getSalesRank() {
		return salesRank;
	}

	public void setSalesRank(int salesRank) {
		this.salesRank = salesRank;
	}

	public ArrayList<String> getSimilar() {
		return similar;
	}

	public void addSimilar(String asin) {
		similar.add(asin);
	}
	
	public void setAvgRating(double avgRating) {
		this.avgRating = avgRating; 
	}
	
	public double getAvgReview() {
		return this.avgRating;
	}
	
	public void setSimilar(ArrayList<String> similar) {
		this.similar = similar;
	}

	public ArrayList<Review> getCustomerTransactions() {
		return customerTransactions;
	}

	public void setCustomerTransactions( Review review) {
		this.customerTransactions.add(review);
	}

	
	

}
