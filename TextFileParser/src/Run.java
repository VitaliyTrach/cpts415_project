import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Run {

	public static void main(String[] args) {

		/*
		 * Parser parser = new Parser();
		 * 
		 * try { parser.readFile();
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		try {
			algorithm();
			//ratingSearch();
			//similaritySearch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Algorithm for our recommendation engine
	public static void algorithm() throws SQLException {

		Database database = new Database();

		String customerID;

		// Taking in user input for the customer they would like to find similar products for
		Scanner in = new Scanner(System.in);
		System.out.println("Which customer would you live to recommend products for? (Enter Customer ID): ");
		customerID = in.nextLine();

		ArrayList<String> larrysProducts;

		// Query that returns all of Larry's products
		larrysProducts = database
				.exeQuery("SELECT asin " + "FROM customer_transaction " + "WHERE cid = '" + customerID + "';");

		// Key: asin, value: list of customers that have left reviews on that asin
		Map<String, ArrayList<String>> Y = new HashMap<String, ArrayList<String>>();

		// Build the Y map
		for (String product : larrysProducts) {
			Y.put(product,
					database.exeQuery("SELECT distinct cid FROM customer_transaction WHERE asin = '" + product + "';"));
		}

		// Key: Customer ID, value: Int that represents how many similar products the customer has to "larry",
		// E.g. the initial customer ID
		Map<String, Integer> counter = new HashMap<String, Integer>();
		
		// Key: Customer ID, value: List of asins that Larry hasn't left a review on (aka potential products)
		Map<String, ArrayList<String>> potentialProducts = new HashMap<String, ArrayList<String>>();

		// For each product in map Y
		for (String key : Y.keySet()) {
			ArrayList<String> alreadyChecked = new ArrayList<String>();

			// For each customer that left a review on the product in Y
			for (String cid : Y.get(key)) {

				if (cid == "ATVPDKIKX0DER")
					continue;

				ArrayList<String> cidProducts = database
						.exeQuery("SELECT asin from customer_transaction where cid = '" + cid + "'");

				// For each product that a similar customer has left a review on
				for (String products : cidProducts) {

					// If larry and cid share the same product
					if (larrysProducts.contains(products) && !alreadyChecked.contains(cid)) {

						alreadyChecked.add(cid);

						// Add to counter hashmap, and increment the counter
						if (!counter.containsKey(cid)) {
							counter.put(cid, 1);
						} else {
							counter.put(cid, counter.get(cid) + 1);
						}

					} 
					// Else if larry and cid don't share the same product
					else {

						// Add to potentialProducts Hashmap
						if (!potentialProducts.containsKey(cid)) {
							potentialProducts.put(cid, new ArrayList<String>());
							potentialProducts.get(cid).add(products);
						} else {
							potentialProducts.get(cid).add(products);
						}
					}

				}
			}
		}

		counter.remove(customerID);

		// Find the customer who shares the most similar products
		String biggestSimilarityCID = "";
		int biggestSimilarity = 0;
		for (String cid : counter.keySet()) {
			if (biggestSimilarity < counter.get(cid)) {
				biggestSimilarity = counter.get(cid);
				biggestSimilarityCID = cid;
			}
		}

		// Print potential products larry might purchase
		for (String asin : potentialProducts.get(biggestSimilarityCID)) {
			System.out.println(asin);
		}
	}

	// Search query to filter based on a rating
	public static void ratingSearch() throws SQLException {
		
		Scanner in = new Scanner(System.in);
		System.out.println("What is the rating you would wish to filter by? (0.0 - 5.0)");
		double rating = in.nextDouble();
		
		System.out.println(" 1 - Books");
		System.out.println(" 2 - Dvds");
		System.out.println(" 3 - Music");
		System.out.println(" 4 - Video");
		
		int optionChosen = in.nextInt();
		String query = null;
		
		do {
			switch (optionChosen) {
		
			case 1:
				query = "SELECT title, asin FROM book WHERE avg_rating >= '" + rating + "'";
				break;
			
			case 2:
				query = "SELECT title, asin FROM dvd WHERE avg_rating >= '" + rating + "'";
				break;
			
			case 3:
				query = "SELECT title, asin FROM music WHERE avg_rating >= '" + rating + "'";
				break;
			
			case 4:
				query = "SELECT title, asin FROM video WHERE avg_rating >= '" + rating + "'";
				break;

			default:
				System.out.println("Wrong value chosen!");
				break;
			}
		} while(query.isEmpty());
		
		Database database = new Database();
		ArrayList<String> results = database.exeQuery(query);
		
		for(int i = 0; i < results.size(); i += 2) {
			System.out.println("Title: " + results.get(i) + "\t Asin: " + results.get(i + 1));
		}
		
	}
	
	// Search query to display similar products of a chosen type, given a asin
	public static void similaritySearch() throws SQLException {
		
		Scanner in = new Scanner(System.in);
		System.out.println("What similar products are you looking for? (Enter asin)");
		String product = in.nextLine();
		
		System.out.println(" 1 - Books");
		System.out.println(" 2 - Dvds");
		System.out.println(" 3 - Music");
		System.out.println(" 4 - Video");
		
		int optionChosen = in.nextInt();
		String query = null;
		
		do {
			switch (optionChosen) {
		
			case 1:
				query = "SELECT DISTINCT b.title, b.asin "
						+ "FROM similarproducts s, book b "
						+ "WHERE s.similarasin = b.asin "
						+ "AND s.asin = '" + product + "';";
				break;
			
			case 2:
				query = "SELECT DISTINCT d.title, d.asin "
						+ "FROM similarproducts s, dvd d "
						+ "WHERE s.similarasin = d.asin "
						+ "AND s.asin = '" + product + "';";
				break;
			
			case 3:
				query = "SELECT DISTINCT m.title, m.asin "
						+ "FROM similarproducts s, music m "
						+ "WHERE s.similarasin = m.asin "
						+ "AND s.asin = '" + product + "';";
				break;
			
			case 4:
				query = "SELECT DISTINCT v.title, v.asin "
						+ "FROM similarproducts s, video v "
						+ "WHERE s.similarasin = v.asin "
						+ "AND s.asin = '" + product + "';";
				break;

			default:
				System.out.println("Wrong value chosen!");
				break;
			}
		} while(query.isEmpty());
		
		Database database = new Database();
		ArrayList<String> results = database.exeQuery(query);
		
		for(int i = 0; i < results.size(); i += 2) {
			System.out.println("Title: " + results.get(i) + "\t Asin: " + results.get(i + 1));
		}
	}
}
