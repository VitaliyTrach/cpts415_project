import java.util.ArrayList;

public class ProductReview {
	private String asin;
	private ArrayList<Review> reviews = new ArrayList<Review>();
	
	
	//GETTERS AND SETTERS
	public void setAsin(String asin) {
		this.asin = asin;
	}
	
	public String getAsin() {
		return asin;
	}

	public void setReviews(Review review) {
		this.reviews.add(review);
	}
	
	public ArrayList<Review> getReviews(){
		return reviews;
	}
}